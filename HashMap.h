#ifndef HASHMAP
#define HASHMAP
#include <vector>
#include <utility>
#include <iostream>
template<typename K,typename V,typename Hash>
class HashMap {
    Hash hashFunction;
	std::vector<std::vector<std::pair<K,V>>> table;
	int sze;
	
public:
    typedef K key_type;
    typedef V mapped_type;
    typedef std::pair<K,V> value_type;
    class const_iterator;
    class iterator {
        decltype(table.begin()) mainIter;
        decltype(table.begin()) mainEnd;
        decltype(table[0].begin()) subIter;
    public:
        friend class const_iterator;

        iterator(const decltype(mainIter) mi,const decltype(mainEnd) me):mainIter(mi),mainEnd(me) {
            if(mainIter!=mainEnd) subIter = mainIter->begin();
            while(mainIter!=mainEnd && subIter == mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
        }
        iterator(const decltype(mainIter) mi,
                const decltype(mainEnd) me,
                const decltype(subIter) si):
                mainIter(mi),mainEnd(me),subIter(si) {}

        bool operator==(const iterator &i) const { return mainIter==i.mainIter && (mainIter==mainEnd || subIter==i.subIter); }
        bool operator!=(const iterator &i) const { return !(*this==i); }
        std::pair<K,V> &operator*() { return *subIter; }
        iterator &operator++() {
            ++subIter;
            while(mainIter!=mainEnd && subIter==mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
            return *this;
        }
        iterator operator++(int) {
            iterator tmp(*this);
            ++(*this);
            return tmp;
        }
    };

    class const_iterator {
        decltype(table.cbegin()) mainIter;
        decltype(table.cbegin()) mainEnd;
        decltype(table[0].cbegin()) subIter;
    public:
        const_iterator(const decltype(table.cbegin()) mi,const decltype(table.cbegin()) me):mainIter(mi),mainEnd(me) {
            if(mainIter!=mainEnd) subIter = mainIter->begin();
            while(mainIter!=mainEnd && subIter == mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
        }
        const_iterator(const decltype(table.begin()) mi,
            const decltype(table.begin()) me,
            const decltype(table[0].begin()) si):
                mainIter(mi),mainEnd(me),subIter(si) {}
        const_iterator(const iterator &i):mainIter(i.mainIter),mainEnd(i.mainEnd),subIter(i.subIter) {

        }
        bool operator==(const const_iterator &i) const { return mainIter==i.mainIter && (mainIter==mainEnd || subIter==i.subIter); }
        bool operator!=(const const_iterator &i) const { return !(*this==i); }
        const std::pair<K,V> &operator*() const { return *subIter; }
        const_iterator &operator++() {
            ++subIter;
            while(mainIter!=mainEnd && subIter==mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
            return *this;
        }
        const_iterator operator++(int) {
            const_iterator tmp(*this);
            ++(*this);
            return tmp;
        }
    };

    HashMap(const Hash &hf):hashFunction{hf},table{10},sze{0}{}
    bool empty() const {
		if(sze==0) {return true;}else {return false;}
	}
    unsigned int size() const {
		return (unsigned int) sze;
	}

    iterator find(const key_type& k){
		auto hashedKey = hashFunction(k);
		auto hashedSlot = hashedKey%table.size();
		auto ret = table.begin()+hashedSlot;
		for(auto x=table[hashedSlot].begin();x!=table[hashedSlot].end();++x){
			if((*x).first == k) {
				return iterator(ret,table.end(),x);
			}
		}
		return end();
	}

    const_iterator find(const key_type& k) const {
		auto hashedKey = hashFunction(k);
		auto hashedSlot = hashedKey%table.size();
		auto ret = table.cbegin()+hashedSlot;
		for(auto x=table[hashedSlot].cbegin();x!=table[hashedSlot].cend();++x){
			if((*x).first == k) {
				return const_iterator(ret,table.cend(),x);
			}
		}
		return end();
				
	}

    int count(const key_type& k) const {
		auto hashedKey = hashFunction(k);
		auto hashedSlot = hashedKey%table.size();
	//	int tmp = 0;
		for(auto x:table[hashedSlot]){
			if(x.first == k) {
				return 1;
			}
		}
		return 0;
	}

    std::pair<iterator,bool> insert(const value_type& val){
		auto hashedKey = hashFunction(val.first);
		if(count(val.first) == 0) {
		if((unsigned int) sze >= (table.size()*0.44)){
			growTableAndRehash();
			auto hashedSlot = hashedKey%table.size();
			table[hashedSlot].push_back(val);
			sze++;	
			return std::pair<iterator,bool>(find(val.first),true);
		} else {
			auto hashedSlot = hashedKey%table.size();
			table[hashedSlot].push_back(val);
			sze++;
			return std::pair<iterator,bool>(find(val.first),true);
		}
		}else {
			return std::pair<iterator,bool>(find(val.first),false);
		}
				
	}

    template <class InputIterator>
    void insert(InputIterator first, InputIterator last) {
        if(first == last) {
			insert(*first);
		}
		for(auto i=first;i!=last;++i){
			insert(*i);	
		}
    }

    iterator erase(const_iterator position) {
		erase((*position).first);
		return find((*(++position)).first);	
	}

    int erase(const key_type& k) {
		auto hashedKey = hashFunction(k);
		auto hashedSlot = hashedKey%table.size();
		for(auto x=table[hashedSlot].begin();x!=(table[hashedSlot].end());++x){
			if((*x).first == k) {
				table[hashedSlot].erase(x);
				sze--;
				return 1;
			}
		}
		return 0;
	}

    void clear() {
		HashMap<K,V,Hash> hashcopy = *this;
		table.clear();
		table.resize(hashcopy.table.size());
		sze = 0;
	}
    mapped_type &operator[](const K &key){
		auto hashedKey = hashFunction(key);
		auto hashedSlot = hashedKey%table.size();
		for(auto x=table[hashedSlot].begin();x!=table[hashedSlot].end();x++){
			if((*x).first == key) {
				return (*x).second;			
			}
		}
			mapped_type vals;
			insert(std::pair<K,V>(key,vals));
			return (table[(hashedKey%table.size())].end()-1)->second;	
		
			
	}

    bool operator==(const HashMap<K,V,Hash>& rhs) const {
	if (size() != rhs.size()){
            return false;
         }
         auto right = begin();
         auto left = rhs.begin();
         while(right != end() && left != rhs.end()){
            if (((*right).first != (*left).first) || ((*right).second != (*left).second)){
                return false;
            }
            ++right;
            ++left;
         }

         return true;		
	}
    bool operator!=(const HashMap<K,V,Hash>& rhs) const {
		return !(*this == rhs);
	}
    iterator begin(){
		return iterator(table.begin(),table.end());
	}
    const_iterator begin() const {
		return const_iterator(table.begin(),table.end());
	}
    iterator end() {
		return iterator(table.end(),table.end());
	}
    const_iterator end() const {
		return const_iterator(table.end(),table.end());
	}
    const_iterator cbegin() const {
		return const_iterator(table.begin(),table.end());
	}
    const_iterator cend() const {
		return const_iterator(table.end(),table.end());
	}
private:
    void growTableAndRehash() {
		HashMap<K,V,Hash> hashcopy = *this;
		table.clear();
		table.resize(1+(hashcopy.table.size()*2));
		sze=0;
		insert(hashcopy.begin(),hashcopy.end());
			
    }
};
#endif
